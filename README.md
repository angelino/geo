# geo

Sistema utilizado para calcular o fluxo medio de pessoas em um estabelecimento em um determinado periodo (MANHÃ/TARDE/NOITE) e dia da semana.

## Dependencias

- Java (JVM) 1.8+
- Clojure 1.10.1
- Leiningen 2.9.1
- Postgres 10.10

## Uso

### 1. Faça o build do projeto utilizando o [Leiningen](https://leiningen.org/).


```sh
    git clone https://angelino@bitbucket.org/angelino/geo.git
    cd <project_dir>/geo
    lein uberjar
```

### 2. Crie e exporte uma variável de ambiente com a URL JDBC para o acesso ao banco de dados no ambiente.

>Obs.: Esta configuração é necessária tanto para que os comandos de criação dos objetos de banco e importação dos datasets quanto o servidor web da API REST funcione adequadamente.


```sh
    export JDBC_DATABASE_URL='postgresql://<user>:<password>@<host>:<port>/<schema>'
```

### 3. Crie o banco de dados.

```sh
    java -jar ./target/geo.jar db:create
```

### 4. Importe os datasets de bairros, populações, concorrentes e eventos.


```sh
    java -jar ./target/geo.jar db:seed
```

Obs.: Caso queira limpar os dados importados, utilize o comando:

```sh
    java -jar ./target/geo.jar db:clean
```

### 5. Inicie o servidor web embarcado utilizando o comando:


```sh
    java -jar target/geo.jar server 3000
```

### 6. Acesse a API utilizando algum cliente como o POSTMAN ou CURL


```sh
    curl http://localhost:3000/api/bairros
    curl http://localhost:3000/api/bairros/{codigo}
    curl http://localhost:3000/api/concorrentes
    curl http://localhost:3000/api/concorrentes/{codigo}/fluxo_eventos
```

## License

Copyright © 2019 FIXME

This program and the accompanying materials are made available under the
terms of the Eclipse Public License 2.0 which is available at
http://www.eclipse.org/legal/epl-2.0.

This Source Code may also be made available under the following Secondary
Licenses when the conditions for such availability set forth in the Eclipse
Public License, v. 2.0 are satisfied: GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or (at your
option) any later version, with the GNU Classpath Exception which is available
at https://www.gnu.org/software/classpath/license.html.
