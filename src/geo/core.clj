(ns geo.core
  (:require [clj-time.core :as t]
            [clj-time.format :as f]))

(defn concorrente [data]
  (update data :codigo-bairro #(or % nil)))

(defn bairro-populado [bairro populacoes]
  (->> (filter #(= (:codigo %) (:codigo bairro)) populacoes)
       (first)
       (merge bairro)))

(defn bairros-populados [bairros populacoes]
  (for [bairro bairros]
    (bairro-populado bairro populacoes)))

(defn densidade-demografica [bairro]
  (when bairro
    (assoc bairro :densidade (double (/ (or (:populacao bairro) 0) (:area bairro))))))

(defn bairro [data]
  (-> data
      (update :codigo #(Long. %))
      (update :area #(Long. (clojure.string/replace % "." "")))))

(defn normalize-datetime [s]
  (first (clojure.string/split s #"\.")))

(defn parse-datetime [s]
  (try
    (f/parse (normalize-datetime s))
    (catch Exception e
      (println "Ignorando formato de data invalido!" s)
      nil)))

(defn ->timestamp [datetime]
  (when datetime
    (new java.sql.Timestamp (.getMillis datetime))))

(def semana {1 :segunda-feira
             2 :terca-feira
             3 :quarta-feira
             4 :quinta-feira
             5 :sexta-feira
             6 :sabado
             7 :domingo})

(defn dia-semana [evento]
  (when-let [dt (:datetime evento)]
    (clojure.string/upper-case (name (get semana (t/day-of-week dt))))))

(defn manha? [evento]
  (when-let [dt (:datetime evento)]
    (#{6 7 8 9 10 11} (t/hour dt))))

(defn tarde? [evento]
  (when-let [dt (:datetime evento)]
    (#{12 13 14 15 16 17} (t/hour dt))))

(defn noite? [evento]
  (when-let [dt (:datetime evento)]
    (#{18 19 20 21 22 23} (t/hour dt))))

(defn periodo [evento]
  (cond
    (manha? evento) "MANHA"
    (tarde? evento) "TARDE"
    (noite? evento) "NOITE"
    :else nil))

(defn evento [data]
  (let [evento (update data :datetime parse-datetime)]
    (assoc evento
           :dia-semana (dia-semana evento)
           :periodo (periodo evento))))







