(ns geo.etl
  (:require [clojure.java.io :as io]
            [clojure.java.jdbc :as jdbc]
            [clojure.data.csv :as csv]
            [cheshire.core :as json]
            [clj-time.core :as t]
            [clj-time.format :as f]
            ;; just require the namespace clj-time.jdbc to register the protocol extensions
            ;; so you don’t have to use clj-time.coerce yourself to coerce to and from SQL timestamps.
            [clj-time.jdbc]
            [geo.core :refer :all]))

(defn normalize-headers [data]
  (map #(keyword (clojure.string/replace % "_" "-")) data))

(defn csv->map [csv-data]
  (let [headers (normalize-headers (first csv-data))
        data    (rest csv-data)]
    (map #(zipmap headers %) data)))

(defn write-csv [filename map-data]
  (with-open [w (io/writer filename :encoding "UTF-8")]
    (let [headers (map name (keys (first map-data)))
          data (map vals map-data)]
      (csv/write-csv w (cons headers data)))))

(defn read-concorrentes []
  (let [concorrentes-parsed (csv/read-csv (slurp (io/resource "concorrentes.csv") :encoding "UTF-8"))]
    (map concorrente (csv->map concorrentes-parsed))))

(defn read-populacoes []
  (json/parse-string (slurp (io/resource "populacao.json") :encoding "UTF-8") keyword))

(defn read-bairros []
  (let [bairros-parsed (csv/read-csv (slurp (io/resource "bairros.csv") :encoding "UTF-8"))]
    (map bairro (csv->map bairros-parsed))))

(defn read-eventos []
  (let [eventos-parsed (csv/read-csv (slurp (io/resource "eventos_de_fluxo.csv") :encoding "UTF-8"))]
    (->> (pmap evento (csv->map eventos-parsed))
         (filter #(some? (:datetime %))))))

;;
;; DB Import!
;; 
(defn import-bairros! [db bairros]
  (println "Importando bairros...")
  (doseq [page (partition-all 1000 bairros)]
    (jdbc/insert-multi! db :bairros page)))

(defn normalize-columns [keys]
  (map #(clojure.string/replace (name %) "-" "_") keys))

(defn import-concorrentes! [db concorrentes]
  (println "Importando concorrentes...")
  (let [columns (normalize-columns (keys (first concorrentes)))]
    (doseq [page (partition-all 1000 concorrentes)]
      (jdbc/insert-multi! db :concorrentes columns (map vals page)))))

(defn import-eventos! [db eventos]
  (println "Importando eventos...")
  (let [columns (normalize-columns (keys (first eventos)))]
    (doseq [page (partition-all 1000 eventos)]
      (jdbc/insert-multi! db :eventos columns (map vals page)))))

(defn simula-importacao-eventos-diaria! [db]
  (let [eventos (read-eventos)]
    (doseq [i (range 60)]
      (println "Importacao" i)
      (import-eventos! db eventos))))

(comment
  (set! *print-length* 100)

  (f/show-formatters)
  (f/formatter)

  (def eventos (read-eventos))
  (def concorrentes (read-concorrentes))
  (def bairros (read-bairros))
  (def populacoes (read-populacoes))

  (count eventos)
  (count concorrentes)
  (count bairros)
  (count populacoes)

  (sort > (set (map #(count (:nome %)) concorrentes)))
  (sort > (set (map #(count (:categoria %)) concorrentes)))
  (sort > (set (map #(count (:endereco %)) concorrentes)))

  (sort > (set (map #(count (:nome %)) bairros)))
  (sort > (set (map #(count (:municipio %)) bairros)))
  (sort > (set (map #(count (:area %)) bairros)))
  (sort > (set (map #(count (:uf %)) bairros)))

  (frequencies (map :faixa-preco concorrentes))
  (frequencies (map #(count (:codigo %)) concorrentes))
  (frequencies (map #(count (:codigo %)) concorrentes))
  (frequencies (map #(count (:codigo %)) bairros))
  (frequencies (map #(count (:codigo %)) eventos))

  (set (map area bairros))

  (keys (first eventos))
  (keys (first concorrentes))
  (keys (first bairros))
  (keys (first populacao-parsed))

  (map #(-> % populacao densidade-demografica) (take 5 bairros))
  (map #(-> % datetime dia-semana) (take 10 eventos))

  (group-by :codigo eventos))
