(ns geo.main
  (:require [clojure.java.io :as io]
            [clojure.java.jdbc :as jdbc]
            [geo.core :refer [bairros-populados]]
            [geo.web :refer [start-server]]
            [geo.etl :as etl]
            [geo.config :as config])
  (:gen-class))

(defn create-objects! []
  (println "Criando objetos no banco de dados...")
  (jdbc/execute! (config/db-spec)
                 (slurp (io/resource "create_objects.sql"))))

(defn drop-objects! []
  (println "Removendo objetos do banco de dados...")
  (jdbc/execute! (config/db-spec)
                 (slurp (io/resource "drop_objects.sql"))))

(defn import-all! []
  (etl/import-bairros! (config/db-spec)
                       (bairros-populados (etl/read-bairros) (etl/read-populacoes)))
  (etl/import-concorrentes! (config/db-spec)
                            (etl/read-concorrentes))
  (etl/import-eventos! (config/db-spec)
                       (etl/read-eventos))
  (println "Fim da importacao!"))

(defn clean-all! []
  (println "Removendo eventos...")
  (println (jdbc/execute! (config/db-spec)
                          ["DELETE FROM eventos"]))
  (println "Removendo concorrentes...")
  (println (jdbc/execute! (config/db-spec)
                          ["DELETE FROM concorrentes"]))
  (println "Removendo bairros...")
  (println (jdbc/execute! (config/db-spec)
                          ["DELETE FROM bairros"])))

(defn print-usage []
  (println "USO:")
  (println "  java -jar geo.jar [COMANDO]")
  (println)
  (println "COMANDO:")
  (println "  server [PORTA] - Inicializa um servidor web na PORTA informada. Se PORTA nao for informada, utiliza a porta 3000 como padrao.")
  (println "  db:create      - Cria os objetos do schema utilizado na aplicacao")
  (println "  db:drop        - WARNING: Apaga os objetos do schema utilizado na aplicacao")
  (println "  db:seed        - Importa os datasets utilizado na aplicacao")
  (println "  db:clean       - WARNING: Apaga todos os dados dos datasets utilizado na aplicacao")
  (println))

(defn -main [& args]
  (let [command (first args)]
    (cond
      (= "server" command) (start-server (Integer/parseInt (or (second args) "3000")))
      (= "db:create" command) (do (create-objects!) (System/exit 0))
      (= "db:drop" command) (do (drop-objects!) (System/exit 0))
      (= "db:seed" command) (do (import-all!) (System/exit 0))
      (= "db:clean" command) (do (clean-all!) (System/exit 0))
      :else (print-usage))))

