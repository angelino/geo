CREATE TABLE IF NOT EXISTS bairros (
       codigo VARCHAR(10) NOT NULL PRIMARY KEY,
       nome VARCHAR(255) NOT NULL, -- 45
       municipio VARCHAR(255) NOT NULL, -- 11
       uf VARCHAR(2) NOT NULL,
       area INTEGER NOT NULL,
       populacao INTEGER, --NOT NULL,
       -- densidade DECIMAL(2,10) NOT NULL,
       created_at TIMESTAMP NOT NULL DEFAULT NOW(),
       updated_at TIMESTAMP NOT NULL DEFAULT NOW()
);

CREATE TABLE IF NOT EXISTS concorrentes (
       codigo VARCHAR(16) NOT NULL PRIMARY KEY,
       nome VARCHAR(255) NOT NULL, -- 73
       categoria VARCHAR(255) NOT NULL, -- 90
       faixa_preco VARCHAR(1) NOT NULL,
       endereco VARCHAR(255) NOT NULL, -- 213
       municipio VARCHAR(255) NOT NULL,
       uf VARCHAR(2) NOT NULL,
       codigo_bairro VARCHAR(10), -- REFERENCES bairros(codigo),
       created_at TIMESTAMP NOT NULL DEFAULT NOW(),
       updated_at TIMESTAMP NOT NULL DEFAULT NOW()
);

CREATE TABLE IF NOT EXISTS eventos (
       codigo VARCHAR(64) NOT NULL, --PRIMARY KEY,
       codigo_concorrente VARCHAR(16) NOT NULL, -- REFERENCES concorrentes(codigo),
       datetime TIMESTAMP NOT NULL,
       dia_semana VARCHAR(13) NOT NULL,
       periodo VARCHAR(10)
);

CREATE VIEW fluxo_eventos AS
  SELECT codigo_concorrente, date(datetime) as data_evento, dia_semana, periodo, count(*) as qtde
  FROM eventos
  GROUP BY codigo_concorrente, date(datetime), dia_semana, periodo;

