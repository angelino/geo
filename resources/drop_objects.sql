--
-- Apaga os objetos criados no script create_objects.sql na ordem inversa da criação.
--
DROP VIEW IF EXISTS fluxo_eventos;
DROP TABLE IF EXISTS eventos;
DROP TABLE IF EXISTS concorrentes;
DROP TABLE IF EXISTS bairros;
