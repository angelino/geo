(defproject geo "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "EPL-2.0 OR GPL-2.0-or-later WITH Classpath-exception-2.0"
            :url "https://www.eclipse.org/legal/epl-2.0/"}
  :dependencies [[org.clojure/clojure "1.10.1"]
                 [org.clojure/data.csv "0.1.4"]
                 [org.clojure/java.jdbc "0.7.10"]
                 [clj-time "0.15.2"]
                 [cheshire "5.9.0"]
                 [http-kit "2.3.0"]
                 [ring/ring-json "0.5.0"]
                 [compojure "1.6.1"]
                 [org.postgresql/postgresql "42.2.6.jre7"]
                 [environ "1.1.0"]]
  :repl-options {:init-ns geo.core}
  :uberjar-name "geo.jar"
  :main geo.main)
  
