(ns geo.web
  (:require [org.httpkit.server :as s]
            [compojure.core :refer [defroutes GET]]
            [compojure.route :as route]
            [ring.middleware.json :refer [wrap-json-response]]
            [clojure.java.jdbc :as jdbc]
            [clojure.pprint :as pp]
            [geo.core :refer :all]
            [geo.config :as config]))

;; Registra conversor para datas do tipo org.joda.time.DateTime
(extend-protocol cheshire.generate/JSONable
  org.joda.time.DateTime
  (to-json [dt gen]
    (cheshire.generate/write-string gen (str dt))))

(defn wrap-db [handler]
  (fn [req]
    (jdbc/with-db-connection [db (config/db-spec)]
      (handler (assoc req :geo/db db)))))

(defn wrap-log-request [handler]
  (fn [req]
    (println)
    (pp/pprint req)
    (handler req)))

(defn ok [body]
  {:status 200
   :headers {}
   :body body})

(defn handle-hello [req]
  {:status  200
   :headers {"Content-Type" "text/html"}
   :body    "Hello Geofusion!"})

(defn handle-index-bairros [req]
  (let [db (:geo/db req)
        bairros (map densidade-demografica (jdbc/query db ["SELECT * FROM bairros"]))]
    (ok bairros)))

(defn handle-show-bairro [req]
  (let [db (:geo/db req)
        codigo (get-in req [:params :codigo])
        bairro (densidade-demografica (first (jdbc/query db ["SELECT * FROM bairros WHERE codigo = ?" codigo])))]
    (ok bairro)))

(defn handle-index-concorrentes [req]
  (let [db (:geo/db req)
        concorrentes (jdbc/query db ["SELECT * FROM concorrentes"])]
    (ok concorrentes)))

(defn handle-show-concorrente [req]
  (let [db (:geo/db req)
        codigo (get-in req [:params :codigo])
        concorrente (first (jdbc/query db ["SELECT * FROM concorrentes WHERE codigo = ?" codigo]))]
    (ok concorrente)))

(defn handle-show-fluxo-eventos [req]
  (let [db (:geo/db req)
        codigo (get-in req [:params :codigo])
        eventos (jdbc/query db [(str "SELECT dia_semana, periodo, count(*)as qtde, sum(qtde) as qtde_total, avg(qtde) as qtde_media"
                                     " FROM fluxo_eventos"
                                     " WHERE periodo is not null"
                                     " AND codigo_concorrente = ?"
                                     " GROUP BY codigo_concorrente, dia_semana, periodo")
                                codigo])]
    (ok eventos)))

(defroutes app-routes
  (GET "/" [] handle-hello)
  (GET "/api/bairros" [] handle-index-bairros)
  (GET "/api/bairros/:codigo" [] handle-show-bairro)
  (GET "/api/concorrentes" [] handle-index-concorrentes)
  (GET "/api/concorrentes/:codigo" [] handle-show-concorrente)
  (GET "/api/concorrentes/:codigo/fluxo_eventos" [] handle-show-fluxo-eventos)
  (route/not-found "<h1>Page not found</h1>"))

(def app
  (-> app-routes
      wrap-db
      wrap-json-response
      wrap-log-request))

(defonce server (atom nil))

(defn stop-server []
  (when-not (nil? @server)
    ;; graceful shutdown: wait 100ms for existing requests to be finished
    ;; :timeout is optional, when no timeout, stop immediately
    (@server :timeout 100)
    (reset! server nil)))

(defn start-server [port]
  ;; The #' is useful when you want to hot-reload code
  ;; You may want to take a look: https://github.com/clojure/tools.namespace
  ;; and http://http-kit.org/migration.html#reload
  (reset! server (s/run-server #'app {:port port}))
  (println @server)
  (println (str "Server started at http://0.0.0.0:" port)))

(comment
  (start-server 3000)
  (stop-server))


