(ns geo.config
  (:require [environ.core :refer [env]]))

(defn db-spec []
  ;; Carrega as configurações do banco de dados do ambiente seguindo a spec
  ;; https://github.com/clojure/java.jdbc#example-usage
  (or (env :jdbc-database-url)
      {:connection-uri (env :db-connection-uri)
       :user (env :db-user)
       :password (env :db-password)}))

(comment
  (def pg-db {:dbtype "postgresql"}
              :dbname "las"
              :host "localhost"
              :user "las"
              :password "las"))
              ;;:ssl true})
              ;;:sslfactory "org.postgresql.ssl.NonValidatingFactory"})
